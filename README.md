Android Firebase Demo<br /><br />
Console : https://console.firebase.google.com/project/fir-demo-47b7d/overview<br />

#1 Firebase Analytics<br />
Firebase Analytics is a free app measurement solution that provides insight on app usage and user engagement.

Docs : https://firebase.google.com/docs/analytics/<br />
Video : https://www.youtube.com/watch?v=iT6EaIwtonY

a) Unlimited Reporting
- Firebase Analytics provides unlimited reporting on up to 500 distinct events.

b) Audience Segmentation
Custom audiences can be defined in the Firebase console based on device data, custom events, or user properties. These audiences can be used with other Firebase features when targeting new features or notifications.

c) Firebase Analytics collects usage and behavior data for your app. The SDK logs two primary types of information:
- Events: What is happening in your app, such as user actions, system events, or errors (maximum events type is 500). 
- User properties: Attributes you define to describe segments of your userbase, such as language preference or geographic location. (maximum custom user properties is 25 / project).

d) Analytics automatically logs some events and user properties; you don't need to add any code to enable them.

![alt tag](https://gitlab.com/bnctvns/my_firebase_demo/raw/d672d021d97e4fa3bb0ba85658d0f459e8a88d99/screenshot/analytics_device.png)
<br />
![alt tag](https://gitlab.com/bnctvns/my_firebase_demo/raw/d672d021d97e4fa3bb0ba85658d0f459e8a88d99/screenshot/analytics_console.png)
<br />
#2 Notifications<br />
Firebase Notifications is a free service that enables targeted user notifications for mobile app developers.

Docs : https://firebase.google.com/docs/notifications/<br />
Video : https://www.youtube.com/watch?v=sioEY4tWmLI

a) Notifications integrates closely with Firebase Analytics, allowing you to target notifications by custom audience. Also, you can target predefined user segments for app, version, and language. With notifications targeted to user segments, you can contact exactly the right user audience for updates on available upgrades, new features, or other news.

b) Notification when App is in background : 
When "notification" key is used, "onMessageReceived" will not be called. In order to receive callback in "onMessageReceived", we should not use "notification" key, instead we only use "data" key for custom payload.

c) Notification when App is in foreground : 
When "notification" key is used, "onMessageReceived" will still be called.

d) Currently Firebase Console cannot send notifications without "notification" key. So when app is in background, it will bring App's launcher activity when user tap on notification at system tray.

![alt tag](https://gitlab.com/bnctvns/my_firebase_demo/raw/d672d021d97e4fa3bb0ba85658d0f459e8a88d99/screenshot/notification_device.png)
<br />
![alt tag](https://gitlab.com/bnctvns/my_firebase_demo/raw/d672d021d97e4fa3bb0ba85658d0f459e8a88d99/screenshot/notification_console.png)
<br />
#3 Authentication<br />
Firebase Authentication provides backend services, easy-to-use SDKs, and ready-made UI libraries to authenticate users to your app. It supports authentication using passwords, popular federated identity providers like Google, Facebook and Twitter, and more.

Docs : https://firebase.google.com/docs/auth/<br />
Video : https://www.youtube.com/watch?v=8sGY55yxicA

a) Federated identity provider integration : 
- Google, Facebook, Twitter, GitHub

![alt tag](https://gitlab.com/bnctvns/my_firebase_demo/raw/d672d021d97e4fa3bb0ba85658d0f459e8a88d99/screenshot/auth_device.png)
<br />
![alt tag](https://gitlab.com/bnctvns/my_firebase_demo/raw/d672d021d97e4fa3bb0ba85658d0f459e8a88d99/screenshot/auth_console.png)
<br />
#4 Remote Config<br />
Change the behavior and appearance of your app without publishing an app update.

Docs : https://firebase.google.com/docs/remote-config/<br />
Video : https://www.youtube.com/watch?v=_CXXVFPO6f0

a) You can make changes to your app's default behavior and appearance by changing server-side parameter values. For example, you could change your app's layout or color theme to support a seasonal promotion, with no need to publish an app update.

b) You can use Remote Config to provide variations on your app's user experience to different segments of your userbase by app version, by Firebase Analytics audience, by language, and more.

![alt tag](https://gitlab.com/bnctvns/my_firebase_demo/raw/d672d021d97e4fa3bb0ba85658d0f459e8a88d99/screenshot/remote_config_device.png)
<br />
![alt tag](https://gitlab.com/bnctvns/my_firebase_demo/raw/d672d021d97e4fa3bb0ba85658d0f459e8a88d99/screenshot/remote_config_console.png)
<br />
#5 Real time database<br />
Store and sync data with our NoSQL cloud database. Data is synced across all clients in realtime, and remains available when your app goes offline.

Docs : https://firebase.google.com/docs/database/<br />
Video : https://www.youtube.com/watch?v=U5aeM5dvUpA

a) Data is stored as JSON and synchronized in realtime to every connected client. When you build cross-platform apps with our iOS, Android, and JavaScript SDKs, all of your clients share one Realtime Database instance and automatically receive updates with the newest data.

![alt tag](https://gitlab.com/bnctvns/my_firebase_demo/raw/d672d021d97e4fa3bb0ba85658d0f459e8a88d99/screenshot/real_time_db_device.png)
<br />
![alt tag](https://gitlab.com/bnctvns/my_firebase_demo/raw/d672d021d97e4fa3bb0ba85658d0f459e8a88d99/screenshot/real_time_db_console.png)
<br />
#6 Crash reporting<br />
Comprehensive and actionable information to help diagnose and fix problems in your app.

Docs : https://firebase.google.com/docs/crash/<br />
Video : https://www.youtube.com/watch?v=B7mlLVAkcfU

a) Errors are grouped into clusters of similar stack traces and triaged by the severity of impact on your users.

![alt tag](https://gitlab.com/bnctvns/my_firebase_demo/raw/d672d021d97e4fa3bb0ba85658d0f459e8a88d99/screenshot/crash_report_console.png)
<br />