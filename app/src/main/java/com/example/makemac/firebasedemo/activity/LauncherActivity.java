package com.example.makemac.firebasedemo.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.example.makemac.firebasedemo.R;
import com.example.makemac.firebasedemo.adapter.FirebaseGridAdapter;
import com.example.makemac.firebasedemo.bean.FirebaseItem;
import com.example.makemac.firebasedemo.configuration.MyFirebaseConfiguration;
import com.example.makemac.firebasedemo.firebase.MyFirebaseRegister;
import com.example.makemac.firebasedemo.service.ServiceFirebase;
import com.example.makemac.firebasedemo.service.impl.ServiceFirebaseImpl;

import java.util.List;

/**
 * Created by makemac on 9/1/16.
 *
 * For display all available menu and getting firebase token device ID for notifications purposes
 */
public class LauncherActivity extends AppCompatActivity {
    public static final String TAG = LauncherActivity.class.getSimpleName();

    // constant for indexing menu
    private static final int FIREBASE_ANALYTICS = 0;
    private static final int FIREBASE_NOTIFICATION = 1;
    private static final int FIREBASE_AUTH = 2;
    private static final int FIREBASE_REMOTE_CONFIG = 3;
    private static final int FIREBASE_REAL_TIME_DB = 4;
    private static final int FIREBASE_CRASH_REPORTING = 5;

    private ServiceFirebase serviceFirebase;
    private MyFirebaseRegister firebaseRegister;
    private BroadcastReceiver tokenReceiver;

    private boolean isRegisteringToken;

    private FirebaseGridAdapter firebaseAdapter;
    private List<FirebaseItem> firebaseItems;
    private RecyclerView rvFirebase;

    private Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);

        serviceFirebase = new ServiceFirebaseImpl(this);
        firebaseRegister = new MyFirebaseRegister(this);

        initView();
        initTokenBroadcastReceiver();
        getFirebaseDeviceToken();
        displayFirebaseItems();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // register broadcast receiver for capturing intent from MyFirebaseInstanceIdService class
        LocalBroadcastManager.getInstance(this).registerReceiver(
                tokenReceiver, new IntentFilter(MyFirebaseConfiguration.KEY_BROADCAST_RECEIVER_TOKEN));
    }

    @Override
    protected void onPause() {
        super.onPause();

        // unregister broadcast receiver for capturing intent from MyFirebaseInstanceIdService class
        LocalBroadcastManager.getInstance(this).unregisterReceiver(tokenReceiver);
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        rvFirebase = (RecyclerView) findViewById(R.id.rv_grid_firebase);
        rvFirebase.setLayoutManager(new GridLayoutManager(this, 2));
        rvFirebase.setHasFixedSize(true);

        setSupportActionBar(toolbar);
    }

    // display grid view for all firebase menu
    private void displayFirebaseItems() {
        firebaseItems = FirebaseItem.getFirebaseItems();
        firebaseAdapter = new FirebaseGridAdapter(this, firebaseItems);
        firebaseAdapter.setClickListener(new AdapterClickHandler());
        rvFirebase.setAdapter(firebaseAdapter);
    }

    private void startNextActivity(int index) {
        Intent intent = null;

        switch (index) {
            case FIREBASE_ANALYTICS:
                intent = new Intent(this, AnalyticsActivity.class);
                break;
            case FIREBASE_AUTH:
                intent = new Intent(this, AuthActivity.class);
                break;
            case FIREBASE_NOTIFICATION:
                intent = new Intent(this, NotificationActivity.class);
                break;
            case FIREBASE_REMOTE_CONFIG:
                intent = new Intent(this, RemoteConfigActivity.class);
                break;
            case FIREBASE_REAL_TIME_DB:
                intent = new Intent(this, RealTimeDBActivity.class);
                break;
            case FIREBASE_CRASH_REPORTING:
                intent = new Intent(this, CrashReportActivity.class);
                break;
        }

        startActivity(intent);
    }

    /**
     * Receiver for capturing "device token" from MyFirebaseInstanceIdService class
     */
    private void initTokenBroadcastReceiver() {
        tokenReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String token = intent.getStringExtra("token");
                registerTokenToServer(token);
            }
        };
    }

    // get firebase device token id
    private void getFirebaseDeviceToken() {
        if (isPlayServiceAvailable()) {
            String token = serviceFirebase.getTokenFromSharedPreferences();
            registerTokenToServer(token);

        } else {
            showNoPlayServicesDialog();
        }
    }

    private boolean isPlayServiceAvailable() {
        boolean isPlayServiceAvailable = firebaseRegister.checkPlayServices();

        return isPlayServiceAvailable;
    }

    private boolean registerTokenToServer(String token) {
        if (token != null) {
            if (!isRegisteringToken) {
                Log.d(TAG, "Token : " + token);

                // here we can register this token to our server
                isRegisteringToken = true;
                Log.d(TAG, "Register Token Success..");

                return true;
            }
        }

        Log.e(TAG, "Token is NULL...");

        return false;
    }

    private void showNoPlayServicesDialog() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.msg_play_services_unavailable)
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, null).create().show();
    }

    private class AdapterClickHandler implements FirebaseGridAdapter.FirebaseAdapterClickListener {

        @Override
        public void onClickItem(int position) {
            startNextActivity(position);
        }
    }
}
