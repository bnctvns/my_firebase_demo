package com.example.makemac.firebasedemo.activity;

import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.makemac.firebasedemo.R;
import com.example.makemac.firebasedemo.configuration.MyFirebaseConfiguration;

/**
 * Created by makemac on 8/31/16.
 */
public class PurchaseActivity extends AppCompatActivity {
    public static final String TAG = PurchaseActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreate called...");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(MyFirebaseConfiguration.NOTIFICATION_ID_ACTION);
        notificationManager.cancel(MyFirebaseConfiguration.NOTIFICATION_ID_ACTION_IMAGE);
    }
}
