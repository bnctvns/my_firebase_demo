package com.example.makemac.firebasedemo.firebase;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by makemac on 8/29/16.
 */
public class MyFirebasePreferences {
    public static final String TAG = MyFirebasePreferences.class.getSimpleName();

    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "leap_firebase";

    // Shared Preferences Keys
    private static final String KEY_TOKEN = "token";
    private static final String KEY_NEED_UPDATE = "need_update";

    // Constructor
    public MyFirebasePreferences(Context context) {
        this.context = context;
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setKeyToken(String token) {
        editor.putString(KEY_TOKEN, token);
        editor.commit();
    }

    public String getKeyToken() {
        return pref.getString(KEY_TOKEN, null);
    }

    public void clear() {
        editor.clear();
        editor.commit();
    }
}