package com.example.makemac.firebasedemo.configuration;

/**
 * Created by makemac on 8/29/16.
 */
public class MyFirebaseConfiguration {
    public static final String TAG = MyFirebaseConfiguration.class.getSimpleName();

    // id to handle the notification in the notification try
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;
    public static final int NOTIFICATION_ID_ACTION = 102;
    public static final int NOTIFICATION_ID_ACTION_IMAGE = 103;

    public static final String KEY_BROADCAST_RECEIVER_TOKEN = "token_received";
}
