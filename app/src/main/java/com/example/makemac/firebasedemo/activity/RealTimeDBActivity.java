package com.example.makemac.firebasedemo.activity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.makemac.firebasedemo.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * Created by makemac on 8/30/16.
 */
public class RealTimeDBActivity extends BaseActivity {
    public static final String TAG = RealTimeDBActivity.class.getSimpleName();

    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;

    private TextView tvNote;
    private EditText etNote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_real_time_db);

        initView();
        initToolbar();
        initFirebaseDatabase();
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvNote = (TextView) findViewById(R.id.tv_note);
        etNote = (EditText) findViewById(R.id.et_note);
        findViewById(R.id.btn_save).setOnClickListener(new ClickHandler());
        findViewById(R.id.btn_clear).setOnClickListener(new ClickHandler());
    }

    private void initFirebaseDatabase() {
        // need to do this before getting firebase database instance
        // FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        firebaseDatabase = FirebaseDatabase.getInstance();

        // the reference is node name in noSQL database
        databaseReference = firebaseDatabase.getReference("note");
        databaseReference.addValueEventListener(new FirebaseValueEventListener());
    }

    private void saveNote() {
        String note = etNote.getText().toString();
        if (note.trim().length() > 0) {
            databaseReference.setValue(note);
        } else {
            databaseReference.setValue("");
        }
    }

    private void clearInput() {
        etNote.setText("");
    }

    private class ClickHandler implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_save:
                    saveNote();
                    break;
                case R.id.btn_clear:
                    clearInput();
                    break;
            }
        }
    }

    private class FirebaseValueEventListener implements ValueEventListener {

        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            // This method is called once with the initial value and again
            // whenever data at this location is updated.
            String value = dataSnapshot.getValue(String.class);
            tvNote.setText(value);
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            // Failed to read value
            Log.w(TAG, "Failed to read value.", databaseError.toException());
        }
    }
}
