package com.example.makemac.firebasedemo.firebase;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.example.makemac.firebasedemo.configuration.MyFirebaseConfiguration;
import com.example.makemac.firebasedemo.service.ServiceFirebase;
import com.example.makemac.firebasedemo.service.impl.ServiceFirebaseImpl;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by makemac on 8/29/16.
 */
public class MyFirebaseInstanceIdService extends FirebaseInstanceIdService {
    public static final String TAG = MyFirebaseInstanceIdService.class.getSimpleName();

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        ServiceFirebase serviceFirebase = new ServiceFirebaseImpl(this);
        serviceFirebase.saveTokenToSharedPreferences(refreshedToken);
        sendBroadcastForTokenDidReceived(refreshedToken);
    }

    private void sendBroadcastForTokenDidReceived(String token) {
        Intent intent = new Intent(MyFirebaseConfiguration.KEY_BROADCAST_RECEIVER_TOKEN);
        intent.putExtra("token", token);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }
}