package com.example.makemac.firebasedemo.customview;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * Created by makemac on 9/1/16.
 */
public class MySquareRelativeLayout extends RelativeLayout {

    private static final double WIDTH_RATIO = 3;
    private static final double HEIGHT_RATIO = 3;

    public MySquareRelativeLayout(Context context) {
        super(context);
    }

    public MySquareRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MySquareRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public MySquareRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightSize = (int) (HEIGHT_RATIO / WIDTH_RATIO * widthSize);
        int newHeightSpec = MeasureSpec.makeMeasureSpec(heightSize, MeasureSpec.EXACTLY);
        super.onMeasure(widthMeasureSpec, newHeightSpec);
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }
}
