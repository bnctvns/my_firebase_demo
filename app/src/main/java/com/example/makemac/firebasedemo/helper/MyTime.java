package com.example.makemac.firebasedemo.helper;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by makemac on 8/29/16.
 */
public class MyTime {
    public static final String TAG = MyTime.class.getSimpleName();

    /**
     * Get current time
     * @return String
     */
    public static String getCurrentTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Calendar cal = Calendar.getInstance();
        String currentTime = dateFormat.format(cal.getTime());

        return currentTime;
    }
}