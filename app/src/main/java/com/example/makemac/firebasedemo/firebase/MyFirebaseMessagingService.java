package com.example.makemac.firebasedemo.firebase;

import android.util.Log;

import com.example.makemac.firebasedemo.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Iterator;
import java.util.Map;

/**
 * Created by makemac on 8/29/16.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {
    public static final String TAG = MyFirebaseMessagingService.class.getSimpleName();

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "On FCM Message Received...");
        createNotification(remoteMessage);
    }

    private void createNotification(RemoteMessage remoteMessage) {
        String notificationMessage = getResources().getString(R.string.app_name);

        // this field is the value we enter on "Message text" on Firebase console
        // this field is inside "notification" key
        if (remoteMessage.getNotification() != null) {
            if (remoteMessage.getNotification().getBody() != null) {
                notificationMessage = remoteMessage.getNotification().getBody();
            }
        }

        // for custom payload in key-value map
        // the key is already defined on Firebase console beforehand
        if (remoteMessage.getData().size() > 0) {
            Map<String, String> mapData = remoteMessage.getData();
            if (mapData.containsKey("notification")) {
                notificationMessage = mapData.get("notification");
            }
            String title = mapData.get("title");
            String author = mapData.get("author");
            String price = mapData.get("price");
            String image = mapData.get("image");

            // Logging purposes
            Iterator it = mapData.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry)it.next();
                Log.d(TAG, pair.getKey() + " = " + pair.getValue());
                it.remove();
            }

            MyFirebaseNotification.fireNotificationWithPayload(
                    this, notificationMessage, title, author, price, image);

        } else {
            MyFirebaseNotification.fireNotification(this, notificationMessage);
        }
    }
}