package com.example.makemac.firebasedemo.firebase;

import android.support.annotation.NonNull;
import android.util.Log;

import com.example.makemac.firebasedemo.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

/**
 * Created by makemac on 9/1/16.
 */
public class MyRemoteConfig {
    public static final String TAG = MyRemoteConfig.class.getSimpleName();

    // the default value is 12 hours
    // so app will not fetch new value from cloud if cached data < 12 hours
    // we set this to 0 to get instant effect (for developing purposes)
    private static final int CACHE_EXPIRATION_TIME = 0;

    private static final String KEY_COLOR_PRIMARY = "color_primary";
    private static final String KEY_COLOR_PRIMARY_DARK = "color_primary_dark";
    private static final String KEY_COLOR_ACCENT = "color_accent";
    private static final String KEY_PROMO_MESSAGE = "promo_message";

    private FirebaseRemoteConfig firebaseRemoteConfig;

    public MyRemoteConfig() {
        initRemoteConfig();
    }

    public String getColorPrimary() {
        return firebaseRemoteConfig.getString(KEY_COLOR_PRIMARY);
    }

    public String getColorPrimaryDark() {
        return firebaseRemoteConfig.getString(KEY_COLOR_PRIMARY_DARK);
    }

    public String getColorAccent() {
        return firebaseRemoteConfig.getString(KEY_COLOR_ACCENT);
    }

    public String getPromoMessage() {
        return firebaseRemoteConfig.getString(KEY_PROMO_MESSAGE);
    }

    public void initRemoteConfig() {
        firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(true)
                .build();
        firebaseRemoteConfig.setConfigSettings(configSettings);
        firebaseRemoteConfig.setDefaults(R.xml.remote_configs_default);
    }

    public void fetchRemoteConfig() {
        firebaseRemoteConfig.fetch(CACHE_EXPIRATION_TIME)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "Fetching remote config success...");
                            firebaseRemoteConfig.activateFetched();
                        } else {
                            Log.e(TAG, "Fetching remote config failed...");
                        }
                    }
                });
    }
}