package com.example.makemac.firebasedemo.service;

/**
 * Created by makemac on 8/29/16.
 */
public interface ServiceFirebase {

    void saveTokenToSharedPreferences(String token);

    String getTokenFromSharedPreferences();
}
