package com.example.makemac.firebasedemo.helper;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v7.app.NotificationCompat;
import android.text.Html;
import android.text.TextUtils;
import android.util.Patterns;

import com.example.makemac.firebasedemo.R;
import com.example.makemac.firebasedemo.configuration.MyFirebaseConfiguration;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by makemac on 8/29/16.
 */
public class NotificationUtils {
    public static final String TAG = NotificationUtils.class.getSimpleName();

    private Context mContext;

    public NotificationUtils(Context mContext) {
        this.mContext = mContext.getApplicationContext();
    }

    // basic notification
    public void showNotificationMessage(final String title, final String message,
                                        final String createdAt, Intent intent) {
        // Check for empty push message
        if (TextUtils.isEmpty(message))
            return;

        // notification icon
        int icon = R.mipmap.ic_launcher;

        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent resultPendingIntent = getPendingIntent(intent);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mContext);
        Uri alarmSound = getAlarmSoundUri();

        showSmallNotification(mBuilder, icon, title, message, createdAt, resultPendingIntent, alarmSound);
        playNotificationSound();
    }

    public void showActionNotificationMessage(final String title, final String message,
                                              final String createdAt, Intent resultIntent, Intent buyIntent,
                                              Intent wishlistIntent, String imageUrl) {
        // Check for empty push message
        if (TextUtils.isEmpty(message))
            return;

        // notification icon
        int icon = R.mipmap.ic_launcher;

        resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent resultPendingIntent = getPendingIntent(resultIntent);

        buyIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent buyPendingIntent = getPendingIntent(buyIntent);

        wishlistIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent wishlistPendingIntent = getPendingIntent(wishlistIntent);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mContext);
        Uri alarmSound = getAlarmSoundUri();

        if (!TextUtils.isEmpty(imageUrl)) {
            if (imageUrl != null && imageUrl.length() > 4 && Patterns.WEB_URL.matcher(imageUrl).matches()) {
                Bitmap bitmap = getBitmapFromURL(imageUrl);
                if (bitmap != null) {
                    showActionImageNotification(bitmap, mBuilder, icon, title, message, createdAt,
                            resultPendingIntent, buyPendingIntent, wishlistPendingIntent, alarmSound);

                } else {
                    showActionNotification(mBuilder, icon, title, message, createdAt,
                            resultPendingIntent, buyPendingIntent, wishlistPendingIntent, alarmSound);
                }
            }

        } else {
            showActionNotification(mBuilder, icon, title, message, createdAt,
                    resultPendingIntent, buyPendingIntent, wishlistPendingIntent, alarmSound);
        }

        playNotificationSound();
    }

    // small notification without image and action button
    private void showSmallNotification(NotificationCompat.Builder mBuilder, int icon, String title,
                                       String message, String createdAt, PendingIntent resultPendingIntent,
                                       Uri alarmSound) {

        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        inboxStyle.addLine(message);

        Notification notification;
        notification = mBuilder.setSmallIcon(icon).setTicker(title).setWhen(0)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setContentIntent(resultPendingIntent)
                .setSound(alarmSound)
                .setStyle(inboxStyle)
                .setWhen(getTimeMilliSec(createdAt))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), icon))
                .setContentText(message)
                .build();

        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(MyFirebaseConfiguration.NOTIFICATION_ID, notification);
    }

    // big notification with action button
    private void showActionNotification(NotificationCompat.Builder mBuilder, int icon, String title,
                                        String message, String createdAt, PendingIntent resultPendingIntent,
                                        PendingIntent buyIntent, PendingIntent wishlistIntent, Uri alarmSound) {

        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
        Notification notification;
        notification = mBuilder.setSmallIcon(icon).setTicker(title)
                .setPriority(Notification.PRIORITY_MAX)
                .setWhen(0)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setContentIntent(resultPendingIntent)
                .setSound(alarmSound)
                .setDefaults(Notification.DEFAULT_ALL)
                .setStyle(bigTextStyle)
                .addAction(R.mipmap.ic_book, mContext.getString(R.string.notif_buy), buyIntent)
                .addAction(R.mipmap.ic_bookmark, mContext.getString(R.string.notif_wishlist), wishlistIntent)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), icon))
                .setContentText(message)
                .build();

        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(MyFirebaseConfiguration.NOTIFICATION_ID_ACTION, notification);
    }

    // big notification with image and action button
    private void showActionImageNotification(Bitmap bitmap, NotificationCompat.Builder mBuilder,
                                             int icon, String title, String message, String createdAt,
                                             PendingIntent resultPendingIntent, PendingIntent buyIntent,
                                             PendingIntent wishlistIntent, Uri alarmSound) {

        NotificationCompat.BigPictureStyle bigPictureStyle = new NotificationCompat.BigPictureStyle();
        bigPictureStyle.setBigContentTitle(title);
        bigPictureStyle.setSummaryText(Html.fromHtml(message).toString());
        bigPictureStyle.bigPicture(bitmap);
        Notification notification;
        notification = mBuilder.setSmallIcon(icon).setTicker(title)
                .setPriority(Notification.PRIORITY_MAX)
                .setWhen(0)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setContentIntent(resultPendingIntent)
                .setSound(alarmSound)
                .setStyle(bigPictureStyle)
                .addAction(R.mipmap.ic_book, mContext.getString(R.string.notif_buy), buyIntent)
                .addAction(R.mipmap.ic_bookmark, mContext.getString(R.string.notif_wishlist), wishlistIntent)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), icon))
                .setContentText(message)
                .build();

        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(MyFirebaseConfiguration.NOTIFICATION_ID_ACTION_IMAGE, notification);
    }

    private PendingIntent getPendingIntent(Intent intent) {
        PendingIntent pendingIntent =
                PendingIntent.getActivity(
                        mContext,
                        0,
                        intent,
                        PendingIntent.FLAG_UPDATE_CURRENT);

        return pendingIntent;
    }

    private Uri getAlarmSoundUri() {
        Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                + "://" + mContext.getPackageName() + "/raw/notification");

        return alarmSound;
    }

    /**
     * Downloading push notification image before displaying it in
     * the notification tray
     */
    public Bitmap getBitmapFromURL(String strURL) {
        try {
            URL url = new URL(strURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void playNotificationSound() {
        try {
            Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                    + "://" + mContext.getPackageName() + "/raw/notification");
            Ringtone r = RingtoneManager.getRingtone(mContext, alarmSound);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static long getTimeMilliSec(String timeStamp) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        try {
            Date date = format.parse(timeStamp);
            return date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }
}