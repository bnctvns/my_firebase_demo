package com.example.makemac.firebasedemo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.makemac.firebasedemo.R;
import com.example.makemac.firebasedemo.helper.MyAlertDialog;
import com.example.makemac.firebasedemo.helper.MyImage;
import com.example.makemac.firebasedemo.helper.MyProgressDialog;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.auth.GoogleAuthProvider;

import java.util.Arrays;

/**
 * Created by makemac on 9/1/16.
 *
 * Firebase auth demo for Google and Facebook sign in
 */
public class AuthActivity extends BaseActivity implements GoogleApiClient.OnConnectionFailedListener {
    public static final String TAG = AuthActivity.class.getSimpleName();

    private static final int RC_SIGN_IN_GOOGLE = 10;

    // Firebase instance
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener firebaseAuthListener;

    // Google Provider instance
    private GoogleSignInOptions gso;
    private GoogleApiClient googleApiClient;

    // Facebook Provider instance
    private CallbackManager callbackManager;

    private TextView tvResult;
    private ImageView ivPhoto;
    private SignInButton btnSignInGoogle;
    private Button btnSignInFacebook;
    private Button btnSignOut;

    private MyProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // must be called before setContentView()
        initFacebookSDK();
        initFacebookListener();
        LoginManager.getInstance().logOut();

        setContentView(R.layout.activity_auth);

        firebaseAuth = FirebaseAuth.getInstance();
        initFirebaseAuthListener();

        initGoogleSignInOptions();
        initGoogleApiClient();
        initView();
        initToolbar();
    }

    @Override
    public void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(firebaseAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (firebaseAuthListener != null) {
            firebaseAuth.removeAuthStateListener(firebaseAuthListener);
        }
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvResult = (TextView) findViewById(R.id.tv_result);
        ivPhoto = (ImageView) findViewById(R.id.iv_photo);
        btnSignInGoogle = (SignInButton) findViewById(R.id.sign_in_button);
        btnSignOut = (Button) findViewById(R.id.sign_out_button);
        btnSignInFacebook = (Button) findViewById(R.id.login_button);
        btnSignInGoogle.setSize(SignInButton.SIZE_STANDARD);

        btnSignInGoogle.setOnClickListener(new ClickHandler());
        btnSignOut.setOnClickListener(new ClickHandler());
        btnSignInFacebook.setOnClickListener(new ClickHandler());

        pDialog = new MyProgressDialog(this);

        showSignInUI();
    }

    private void initFacebookSDK() {
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
    }

    private void initGoogleSignInOptions() {
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.firebase_web_client_id))
                .requestEmail()
                .build();
    }

    private void initGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    private void initFacebookListener() {
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {

                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        firebaseAuthWithFacebook(loginResult);
                    }

                    @Override
                    public void onCancel() {
                        MyAlertDialog.simpleAlertDialog(AuthActivity.this, "Login attempt canceled");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        MyAlertDialog.simpleAlertDialog(AuthActivity.this, "Login attempt failed");
                    }
                });
    }

    private void signInWithGoogle() {
        Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(intent, RC_SIGN_IN_GOOGLE);
    }

    private void signInWithFacebook() {
        LoginManager.getInstance().logInWithReadPermissions(
                AuthActivity.this, Arrays.asList("user_status, public_profile, email"));
    }

    private void signOut() {
        signOutGoogle();
        signOutFacebook();
    }

    private void signOutGoogle() {
        if (googleApiClient.isConnected()) {
            Auth.GoogleSignInApi.revokeAccess(googleApiClient).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status status) {
                            firebaseAuth.signOut();
                            showSignInUI();
                        }
                    });
        }
    }

    public void signOutFacebook() {
        if (AccessToken.getCurrentAccessToken() == null) {
            Log.d(TAG, "facebook already logout");
            showSignInUI();
            return; // already logged out
        }

        pDialog.showpDialog();
        new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null,
                HttpMethod.DELETE, new GraphRequest
                .Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {
                LoginManager.getInstance().logOut();
                showSignInUI();
                pDialog.hidepDialog();

            }
        }).executeAsync();
    }

    private void handleSignInFirebaseResult(FirebaseUser firebaseUser) {
        String uid = firebaseUser.getUid();
        String token = getFirebaseUserToken(firebaseUser);
        String username = firebaseUser.getDisplayName();
        String email = firebaseUser.getEmail();
        String photo = firebaseUser.getPhotoUrl().toString();
        String providerID = firebaseUser.getProviderId();

        Log.d(TAG, "UID : " + uid);
        Log.d(TAG, "Token : " + token);
        Log.d(TAG, "username : " + username);
        Log.d(TAG, "email : " + email);
        Log.d(TAG, "photo : " + photo);
        Log.d(TAG, "provider ID : " + providerID);

        tvResult.setText(
                "UID : " + uid + "\n" +
                        "Username : " + username + "\n" +
                        "Email : " + email + "\n"
        );

        MyImage.loadCircleImage(this, photo, ivPhoto);
        showSignOutUI();
        pDialog.hidepDialog();
    }

    // get firebase user token if the backend app need to verify user
    private String getFirebaseUserToken(final FirebaseUser firebaseUser) {
        FirebaseTokenHandler firebaseTokenHandler = new FirebaseTokenHandler();
        firebaseUser.getToken(true).addOnCompleteListener(firebaseTokenHandler);

        // send this token to backend app
        String token = firebaseTokenHandler.token;

        return token;
    }

    private void showSignInFailedDialog(String message) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, null).create().show();
    }

    private void showSignInUI() {
        btnSignInGoogle.setVisibility(View.VISIBLE);
        btnSignInFacebook.setVisibility(View.VISIBLE);
        btnSignOut.setVisibility(View.GONE);
        tvResult.setText("");
        ivPhoto.setImageDrawable(null);
    }

    private void showSignOutUI() {
        btnSignInGoogle.setVisibility(View.GONE);
        btnSignInFacebook.setVisibility(View.GONE);
        btnSignOut.setVisibility(View.VISIBLE);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(TAG, "Google Connection Failed : " + connectionResult.getErrorMessage());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
            if (requestCode == RC_SIGN_IN_GOOGLE) {
                GoogleSignInResult signInResult = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                firebaseAuthWithGoogle(signInResult);

            } else {
                Log.d(TAG, String.valueOf(requestCode) + " request code for Facebook called " + data.getClass());
                callbackManager.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    // this listener will be called for every change in firebase auth state
    private void initFirebaseAuthListener() {
        firebaseAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
                if (firebaseUser != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged : signed_in :" + firebaseUser.getUid());
                    handleSignInFirebaseResult(firebaseUser);

                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged : signed_out");
                }
            }
        };
    }

    // exchange google token with firebase credentials
    private void firebaseAuthWithGoogle(GoogleSignInResult signInResult) {
        if (signInResult.isSuccess()) {
            GoogleSignInAccount account = signInResult.getSignInAccount();
            final AuthCredential authCredential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
            firebaseAuth.signInWithCredential(authCredential).addOnCompleteListener(
                    this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            // will automatically fire callback on firebase auth listener

                            if (!task.isSuccessful()) {
                                pDialog.hidepDialog();
                                Log.e(TAG, "Firebase Google auth exception : " + task.getException());
                                showSignInFailedDialog("Authentication failed\n\n" + task.getException());
                            }
                        }
                    });

        } else {
            Log.e(TAG, "Firebase auth failed...");
        }
    }

    // exchange facebook token with firebase credentials
    private void firebaseAuthWithFacebook(LoginResult loginResult) {
        AccessToken accessToken = loginResult.getAccessToken();
        final AuthCredential credential = FacebookAuthProvider.getCredential(accessToken.getToken());
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        // will automatically fire callback on firebase auth listener

                        if (!task.isSuccessful()) {
                            Log.e(TAG, "Firebase Facebook auth exception : " + task.getException());
                            showSignInFailedDialog("Authentication failed\n\n" + task.getException());
                        }
                    }
                });
    }

    private class FirebaseTokenHandler implements OnCompleteListener<GetTokenResult> {

        private String token = "";

        public void onComplete(@NonNull Task<GetTokenResult> task) {
            if (task.isSuccessful()) {
                token = task.getResult().getToken();
            }
        }
    }

    private class ClickHandler implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.sign_in_button:
                    signInWithGoogle();
                    break;
                case R.id.login_button:
                    signInWithFacebook();
                    break;
                case R.id.sign_out_button:
                    signOut();
                    break;
            }
        }
    }
}
