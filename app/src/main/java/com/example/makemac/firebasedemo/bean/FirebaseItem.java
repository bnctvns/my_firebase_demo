package com.example.makemac.firebasedemo.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by makemac on 9/1/16.
 */
public class FirebaseItem {

    private String title;
    private String color;

    public FirebaseItem(String title, String color) {
        this.title = title;
        this.color = color;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public static List<FirebaseItem> getFirebaseItems() {
        List<FirebaseItem> firebaseItems = new ArrayList<>();

        firebaseItems.add(new FirebaseItem("Analytics", "#F44336"));
        firebaseItems.add(new FirebaseItem("Notifications", "#E91E63"));
        firebaseItems.add(new FirebaseItem("Authentication", "#9C27B0"));
        firebaseItems.add(new FirebaseItem("Remote Config", "#673AB7"));
        firebaseItems.add(new FirebaseItem("Real Time Database", "#3F51B5"));
        firebaseItems.add(new FirebaseItem("Crash Reporting", "#2196F3"));

        return firebaseItems;
    }
}
