package com.example.makemac.firebasedemo.firebase;

import android.content.Context;
import android.content.Intent;

import com.example.makemac.firebasedemo.R;
import com.example.makemac.firebasedemo.activity.NotificationActivity;
import com.example.makemac.firebasedemo.activity.PurchaseActivity;
import com.example.makemac.firebasedemo.activity.WishlistActivity;
import com.example.makemac.firebasedemo.helper.MyTime;
import com.example.makemac.firebasedemo.helper.NotificationUtils;

/**
 * Created by makemac on 8/29/16.
 */
public class MyFirebaseNotification {
    public static final String TAG = MyFirebaseNotification.class.getSimpleName();

    // trigger notification without payload
    public static void fireNotification(Context context, String message) {
        Intent resultIntent = new Intent(context, NotificationActivity.class);
        resultIntent.putExtra("notification", message);

        fireNotification(context, message, resultIntent);
    }

    // trigger notification with payload data
    public static void fireNotificationWithPayload(Context context, String notificationMessage,
                                                   String title, String author, String price, String image) {

        Intent resultIntent = new Intent(context, NotificationActivity.class);
        resultIntent.putExtra("notification", notificationMessage);
        resultIntent.putExtra("title", title);
        resultIntent.putExtra("author", author);
        resultIntent.putExtra("price", price);
        resultIntent.putExtra("image", image);

        Intent buyIntent = new Intent(context, PurchaseActivity.class);
        Intent wishlistIntent = new Intent(context, WishlistActivity.class);

        fireActionNotification(context, notificationMessage, image, resultIntent,
                buyIntent, wishlistIntent);
    }

    // trigger usual notification
    private static void fireNotification(Context context, String notificationMessage, Intent intent) {
        NotificationUtils notificationUtils = new NotificationUtils(context);
        notificationUtils.showNotificationMessage(
                context.getResources().getString(R.string.app_name),
                notificationMessage,
                MyTime.getCurrentTime(),
                intent);
    }

    // trigger notification with action button
    private static void fireActionNotification(Context context, String notificationMessage,
                                               String notificationImage, Intent resultIntent,
                                               Intent buyIntent, Intent wishlistIntent) {
        NotificationUtils notificationUtils = new NotificationUtils(context);
        notificationUtils.showActionNotificationMessage(
                context.getResources().getString(R.string.app_name),
                notificationMessage,
                MyTime.getCurrentTime(),
                resultIntent,
                buyIntent,
                wishlistIntent,
                notificationImage);
    }
}