package com.example.makemac.firebasedemo.helper;

import android.content.Context;
import android.support.v7.app.AlertDialog;

/**
 * Created by makemac on 9/1/16.
 */
public class MyAlertDialog {

    public static void simpleAlertDialog(Context context, String message) {
        new AlertDialog.Builder(context)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, null).create().show();
    }
}
