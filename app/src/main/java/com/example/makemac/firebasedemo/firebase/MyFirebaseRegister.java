package com.example.makemac.firebasedemo.firebase;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

/**
 * Created by makemac on 8/29/16.
 *
 * If we need to register the firebase device token to our backend app
 */
public class MyFirebaseRegister {
    public static final String TAG = MyFirebaseRegister.class.getSimpleName();

    private final int PLAY_SERVICES_RESOLUTION_REQUEST = 99;

    // activity context
    private Context context;

    public MyFirebaseRegister(Context context) {
        this.context = context;
    }

    public boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(context);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog((AppCompatActivity) context, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported. Google Play Services not installed !");
            }

            return false;
        }

        return true;
    }
}