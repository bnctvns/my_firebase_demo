package com.example.makemac.firebasedemo.service.impl;

import android.content.Context;

import com.example.makemac.firebasedemo.firebase.MyFirebasePreferences;
import com.example.makemac.firebasedemo.service.ServiceFirebase;

/**
 * Created by makemac on 8/29/16.
 */
public class ServiceFirebaseImpl implements ServiceFirebase {
    public static final String TAG = ServiceFirebaseImpl.class.getSimpleName();

    private MyFirebasePreferences firebasePreferences;

    public ServiceFirebaseImpl(Context context) {
        this.firebasePreferences = new MyFirebasePreferences(context.getApplicationContext());
    }

    @Override
    public void saveTokenToSharedPreferences(String token) {
        firebasePreferences.setKeyToken(token);
    }

    @Override
    public String getTokenFromSharedPreferences() {
        return firebasePreferences.getKeyToken();
    }
}
