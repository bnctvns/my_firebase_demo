package com.example.makemac.firebasedemo.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.example.makemac.firebasedemo.R;
import com.example.makemac.firebasedemo.helper.MyAlertDialog;
import com.google.firebase.crash.FirebaseCrash;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by makemac on 9/1/16.
 */
public class CrashReportActivity extends BaseActivity {
    public static final String TAG = CrashReportActivity.class.getSimpleName();

    private Button btnNonFatalException;
    private Button btnFatalException;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crash_report);

        initView();
        initToolbar();
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        btnNonFatalException = (Button) findViewById(R.id.btn_non_fatal_exception);
        btnFatalException = (Button) findViewById(R.id.btn_fatal_exception);

        btnNonFatalException.setOnClickListener(new ClickHandler());
        btnFatalException.setOnClickListener(new ClickHandler());
    }

    private void createNonFatalException() {
        // generate non fatal exception
        FirebaseCrash.report(new Exception("My app Exception"));
        MyAlertDialog.simpleAlertDialog(this, "Sent Non Fatal Exception");
    }

    private void createFatalException() {
        // generate array index out of bound exception -> Fatal
        List<String> listString = new ArrayList<>();
        listString.get(0);
    }

    private class ClickHandler implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_non_fatal_exception:
                    createNonFatalException();
                    break;
                case R.id.btn_fatal_exception:
                    createFatalException();
                    break;
            }
        }
    }
}
