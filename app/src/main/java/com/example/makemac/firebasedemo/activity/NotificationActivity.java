package com.example.makemac.firebasedemo.activity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.makemac.firebasedemo.R;

/**
 * Created by makemac on 8/31/16.
 */
public class NotificationActivity extends BaseActivity {
    public static final String TAG = NotificationActivity.class.getSimpleName();

    private TextView tvNotification, tvTitle, tvAuthor, tvPrice, tvNoNotif;
    private String notification, title, author, price;

    // get intent from previous activity if notifications happen when app is in background
    // when app is in background, the payload is delivered through intent to App's launcher activity by default
    // because tap event on notification at system tray will open App's launcher activity
    private boolean getIntentFromPrevActivity() {
        Bundle extras = getIntent().getExtras();

        if (extras != null) {

            // get custom payload that already defined on Firebase Console
            notification = extras.getString("notification");
            title = extras.getString("title");
            author = extras.getString("author");
            price = extras.getString("price");

            Log.d(TAG, "notification message : " + notification);

            return true;
        }

        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate called...");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        initView();
        initToolbar();

        if (getIntentFromPrevActivity()) {
            displayContent();
        }
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvNotification = (TextView) findViewById(R.id.tv_notification);
        tvTitle = (TextView) findViewById(R.id.tv_title);
        tvAuthor = (TextView) findViewById(R.id.tv_author);
        tvPrice = (TextView) findViewById(R.id.tv_price);
        tvNoNotif = (TextView) findViewById(R.id.tv_no_notif);
    }

    private void displayContent() {
        tvNoNotif.setVisibility(View.GONE);

        if (notification != null)
            tvNotification.setText(notification);

        if (title != null)
            tvTitle.setText("Book Title : " + title);

        if (author != null)
            tvAuthor.setText("Book Author : " + author);

        if (price != null)
            tvPrice.setText("Book Price : $ " + price);
    }
}
