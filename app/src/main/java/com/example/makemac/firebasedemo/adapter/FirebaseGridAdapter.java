package com.example.makemac.firebasedemo.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.makemac.firebasedemo.R;
import com.example.makemac.firebasedemo.bean.FirebaseItem;
import com.example.makemac.firebasedemo.customview.MySquareRelativeLayout;

import java.util.List;

/**
 * Created by makemac on 9/1/16.
 */
public class FirebaseGridAdapter extends RecyclerView.Adapter<FirebaseGridAdapter.ViewHolder> {
    public static final String TAG = FirebaseGridAdapter.class.getSimpleName();

    private Context context;
    private List<FirebaseItem> firebaseItems;
    private static FirebaseAdapterClickListener clickListener;

    public FirebaseGridAdapter(Context context, List<FirebaseItem> firebaseItems) {
        this.context = context;
        this.firebaseItems = firebaseItems;
    }

    public static void setClickListener(FirebaseAdapterClickListener clickListener) {
        FirebaseGridAdapter.clickListener = clickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.row_firebase_grid, parent, false);
        ViewHolder vh = new ViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        FirebaseItem item = firebaseItems.get(position);

        holder.tvTitle.setText(item.getTitle());
        holder.ivBackground.setBackgroundColor(Color.parseColor(item.getColor()));
    }

    @Override
    public int getItemCount() {
        return firebaseItems.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private MySquareRelativeLayout containerRow;
        private ImageView ivBackground;
        private TextView tvTitle;

        public ViewHolder(View v) {
            super(v);

            containerRow = (MySquareRelativeLayout) v.findViewById(R.id.root_container_row);
            ivBackground = (ImageView) v.findViewById(R.id.iv_background);
            tvTitle = (TextView) v.findViewById(R.id.tv_title);

            containerRow.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.root_container_row) {
                clickListener.onClickItem(getLayoutPosition());
            }
        }
    }

    public interface FirebaseAdapterClickListener {
        void onClickItem(int position);
    }
}
