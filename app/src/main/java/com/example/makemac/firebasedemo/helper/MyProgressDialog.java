package com.example.makemac.firebasedemo.helper;

import android.app.ProgressDialog;
import android.content.Context;

import com.example.makemac.firebasedemo.R;

/**
 * Created by makemac on 9/1/16.
 */
public class MyProgressDialog {
    public static final String TAG = MyProgressDialog.class.getSimpleName();

    private ProgressDialog pDialog;

    public MyProgressDialog(Context context) {
        pDialog = new ProgressDialog(context);
        pDialog.setMessage(context.getString(R.string.msg_loading));
        pDialog.setCancelable(false);
    }

    public void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    public void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}