package com.example.makemac.firebasedemo.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.example.makemac.firebasedemo.R;
import com.example.makemac.firebasedemo.firebase.MyRemoteConfig;
import com.example.makemac.firebasedemo.helper.MyAlertDialog;

/**
 * Created by makemac on 9/1/16.
 */
public class RemoteConfigActivity extends BaseActivity {
    public static final String TAG = RemoteConfigActivity.class.getSimpleName();

    private MyRemoteConfig remoteConfig;

    private View viewTop, viewMiddle, viewBottom;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remote_config);

        remoteConfig = new MyRemoteConfig();
        remoteConfig.fetchRemoteConfig();

        initView();
        initToolbar();
        showPromoMessage();
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        viewTop = findViewById(R.id.view_top);
        viewMiddle = findViewById(R.id.view_middle);
        viewBottom = findViewById(R.id.view_bottom);

        viewTop.setBackgroundColor(Color.parseColor(remoteConfig.getColorPrimaryDark()));
        viewMiddle.setBackgroundColor(Color.parseColor(remoteConfig.getColorPrimary()));
        viewBottom.setBackgroundColor(Color.parseColor(remoteConfig.getColorAccent()));
    }

    // we can even get custom value from cloud with condition statement on Firebase console
    // based on audience / user attributes
    private void showPromoMessage() {
        String promoMessage = remoteConfig.getPromoMessage();
        if (promoMessage != null && promoMessage.trim().length() > 0) {
            Log.d(TAG, "Promo Message : " + promoMessage);
            MyAlertDialog.simpleAlertDialog(this, promoMessage);
        }
    }
}
