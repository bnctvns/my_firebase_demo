package com.example.makemac.firebasedemo.activity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.makemac.firebasedemo.R;
import com.example.makemac.firebasedemo.helper.MyAlertDialog;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by makemac on 8/29/16.
 */
public class AnalyticsActivity extends BaseActivity {
    public static final String TAG = AnalyticsActivity.class.getSimpleName();

    private FirebaseAnalytics firebaseAnalytics;

    // viewgroup for buttons
    private LinearLayout container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_analytics);

        firebaseAnalytics = FirebaseAnalytics.getInstance(this);

        initView();
        initToolbar();
        createDynamicButton();
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        container = (LinearLayout) findViewById(R.id.container);
    }

    private void createDynamicButton() {
        List<String> attributes = new ArrayList<>();
        for (int i = 1; i < 51; i++) {
            attributes.add("Buku " + String.valueOf(i));
        }

        for (String attribute : attributes) {
            ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(650, ViewGroup.LayoutParams.WRAP_CONTENT);
            Button button = new Button(this);
            button.setLayoutParams(params);
            button.setText(attribute);
            button.setOnClickListener(new ClickHandler());
            container.addView(button);
        }
    }

    private void setAnalyticsUserProperties(String key, String field) {
        // set and send user property automatically
        firebaseAnalytics.setUserProperty(key, field);

        // set and send event automatically
        Bundle params = new Bundle();
        params.putString("name", field);
        firebaseAnalytics.logEvent("event_book_name", params);

        String message = "Sending User Attribute and Event Name : \n\n" + field +
                "\n\nCheck the event and user attribute on Firebase Console";
        MyAlertDialog.simpleAlertDialog(this, message);
    }

    private class ClickHandler implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            String field = ((Button) view).getText().toString();
            setAnalyticsUserProperties("book_name", field);
        }
    }
}
